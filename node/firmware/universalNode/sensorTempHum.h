#include "config.h"
#ifdef SENSOR_TEMP_HUM
#pragma once

#include "device.h"
#include "config_board.h"
#include "DHT.h"

class SensorTempHum : public Device
{
private:
DHT* dht = new DHT(DHTPIN,DHTTYPE);
public:
    SensorTempHum(int aDeviceID);
    ~SensorTempHum();
    void handle(Payload *message = NULL); 
};
#endif
