#include "config.h"
#ifdef I2CMETER
#include "sensorI2CMeter.h"
#include "utils.h"
#include "config_board.h"
#include "radio.h"

SensorI2CMeter::SensorI2CMeter(int aDeviceID): Device(aDeviceID)
{
    blinkNTimes(3);
    Wire.begin();
    this->handle();
    return;
}


void SensorI2CMeter::handle(Payload *message)
{  
    for (int i = 1; i <= I2CMETER_DEVICES; i++)
    {
        uint64_t I2C_data;

        byte a,b,c,d,e,f,g,h;
        
        Wire.requestFrom(i, 8); // request 8 Bytes from slave with I2C adress i
        
        //read all 8 Bytes
        a = Wire.read(); 
        b = Wire.read();
        c = Wire.read();
        d = Wire.read();
        e = Wire.read();
        f = Wire.read();
        g = Wire.read();
        h = Wire.read();

        //put all 8 Bytes back together
        I2C_data = a;
        I2C_data = (I2C_data << 8) | b;
        I2C_data = (I2C_data << 8) | c;
        I2C_data = (I2C_data << 8) | d;
        I2C_data = (I2C_data << 8) | e;
        I2C_data = (I2C_data << 8) | f;
        I2C_data = (I2C_data << 8) | g;
        I2C_data = (I2C_data << 8) | h;

        //Calculate actual Meter Value
        float Meter_Reading = (float(I2C_data) / 1000);
        
        if (debug)
        {
            Serial.print("I2C-Slave mit ID ");
            Serial.print(i);
            Serial.print(" ausgelesen. Zählerstand: ");
            Serial.println(Meter_Reading);
        }

        //Send the Meter reading to the Gateway
        radio->Send(I2CMETER+i,Meter_Reading)
        blinkNTimes(1);
    }
    radio->Sleep();
}
#endif