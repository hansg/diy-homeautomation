#include "config.h"
#ifdef SENSOR_DISTANCE
#pragma once

#include <HCSR04.h>
#include "device.h"

class SensorDistance : public Device
 {
  private:
    HCSR04* sensor;

  public:
    SensorDistance(int aDeviceID);
    ~SensorDistance();
    void handle(Payload *message = NULL); 
};
#endif