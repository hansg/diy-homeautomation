#include "config.h"
#ifdef DEKO_ELEMENT

#pragma once

#include "device.h"
#include "radio.h"

class ActorDeko : public Device
{

public:
    ActorDeko(int aDeviceID);
    ~ActorDeko();
    void handle(Payload *message = NULL);

};
#endif