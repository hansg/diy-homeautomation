#include "Arduino.h"
#include <Wire.h>;


//------------------------------------------------------------------------------------------------------------------------------------------------
#define I2C_ID 1  //Die ID für die I2C-Slaves sollte von 1 beginnend aufsteigend durchnummeriert werden, sonst kann der Master diese nicht auslesen
//------------------------------------------------------------------------------------------------------------------------------------------------


bool debug = true;

byte  serial_data;
String inString;
float   D0_meter_Reading;
float   D0_current_Power;
unsigned long time_1;
unsigned long time_2;


void setup() {
  Serial.begin(9600);
  if (debug) Serial.println("Serielle Kommunikation gestartet");
  Wire.begin(I2C_ID);
  Wire.onRequest(Daten_senden);
  if (debug) Serial.println("I2C Kommunikation gestartet");
}

void loop() {
  time_1 = millis();
  time_2 = millis();
  D0_meter_Reading = -1;
  D0_current_Power = -1;
  //wait for some seconds:
  if (debug) Serial.println("start waiting for D0 serial data");
  while ((time_1 + 8000) > time_2) { //waiting for 8 Seconds
    time_2 = millis();
    while (Serial.available() > 0) {
      //mySerial.println("1");
      //read data from serial interface
      serial_data = Serial.read();
      //convert byteformate from 7E1 to 8N1 bye masking parity bit (alternative: use Serial.begin(9600, SERIAL_7E1) when starting serial port, but debug is then not available over Arduino IDE Serial Monitor)
      serial_data = serial_data & 0b01111111;

      //check for end of line
      if (serial_data != '\n') {
        inString += (char)serial_data;
      }
      else {
        //Return the read Data for Error spotting
        if (debug) Serial.println(inString);

        //Check for Meter Reading under code 1-0:1.8.0
        if (inString.indexOf("1-0:1.8.0") != -1) {
          //Get meter reading from string and convert to float
          D0_meter_Reading = (inString.substring(14, 30)).toFloat();
          if (debug) { //debug messages
            Serial.print("Digital meter reading: ");
            Serial.println(D0_meter_Reading);
          }
        }
        if (inString.indexOf("1-0:1.7.255") != -1) {
          //Serial.println("5");
          //Get current Power from string and convert to float
          D0_current_Power = (inString.substring(16, 25)).toFloat();
          if (debug) { //debug messages
            Serial.print("current power output: ");
            Serial.println(D0_current_Power);
          }
        }
        inString = "";
      }
    }
  }
  if (debug) Serial.println("end waiting for D0 serial Data");
}

void Daten_senden(){

  uint64_t data = uint64_t(round(D0_meter_Reading*1000));
  byte data_array[8];

  data_array[0] = (data >> 56) & 0xFF;
  data_array[1] = (data >> 48) & 0xFF;
  data_array[2] = (data >> 40) & 0xFF;
  data_array[3] = (data >> 32) & 0xFF;
  data_array[4] = (data >> 24) & 0xFF;
  data_array[5] = (data >> 16) & 0xFF;
  data_array[6] = (data >> 8) & 0xFF;
  data_array[7] = data & 0xFF;
  
  Wire.write(data_array, 8);
}
